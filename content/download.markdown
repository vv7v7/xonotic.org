---
author: detrate
comments: false
date: 2010-12-23 20:18:51+00:00
slug: download
title: Download
wordpress_id: 839
type: page
---

### LATEST RELEASE: Xonotic 0.8.2 for Linux, Windows, and Mac

[<i class="fa fa-download"></i> Download as zip](http://dl.xonotic.org/xonotic-0.8.2.zip)  
[<i class="fa fa-download"></i> Download via torrent](http://dl.xonotic.org/xonotic-0.8.2.zip.torrent)

No installation required, just unpack and run. Got questions? See the [FAQ](/faq) or [live chat](/chat) with us.

sha256sum: a22f7230f486c5825b55cfdadd73399c9b0fae98c9e081dd8ac76eca08359ad5  
shasum: 9a1726e3d0d4e5e23c1e799734397c63e5df6ec9  
MD5: 1bd46c1fb79aae42bb13e74f5a0ff46e  
Package size: 946M  

### Hardware requirements

**Minimum (low settings):**  
1.6 GHz single-core CPU  
2 GB RAM  
onboard GPU  

**Optimal (60 fps on normal settings):**  
2.5 GHz dual-core CPU (for background tasks - Xonotic is single threaded)  
4 GB RAM  
GPU with 512 MB VRAM  

Linux or OSX or Windows XP and newer  

### Upgrading from an older version

Since Xonotic version 0.5 we've offered an auto-update tool that will seamlessly upgrade your client to the latest release, provided you're using an official release or autobuild to begin with. You can find this tool in your Xonotic application directory. To use it, start by navigating to the misc->tools->rsync-updater subdirectory, then double-click (or otherwise execute in a terminal) the "update-to-release.sh" inside this directory. There is also a "update-to-release.bat" script for Windows users. This script will examine your installation and update only the files needed to bring your copy up to date using the "rsync" tool. In no time you'll have a shiny new client with which to play!

### Extra downloads

**[Download NetRadiant via HTTP](http://dl.xonotic.org/xonotic-0.8.2-mappingsupport.zip)**  
**Description:** Official Xonotic map editor with all the required textures to create maps. The download only contains binaries for Windows and OSX, Linux users will need to compile from source- which is also provided in the package.  

**[Lower quality download via HTTP](http://dl.xonotic.org/xonotic-0.8.2-low.zip)**  
**Description:** Release build with JPEG texture compression instead of using DDS textures compiled with S3TC. This build has smaller file size and has better support for opensource/legacy drivers, but the textures take slightly longer while loading the game.  

**[Source download via HTTP](http://dl.xonotic.org/xonotic-0.8.2-source.zip)**  
**Description:** Source of all code parts (also included with the other downloads).  

**[Our project page on GitLab](https://gitlab.com/groups/xonotic)**

### Older downloads  
[Download 0.8.1 via torrent](http://dl.xonotic.org/xonotic-0.8.1.zip.torrent) (940M, md5: f7a9cd8ab68a00336acca164f983b569)  
[Download 0.8.1 via HTTP](http://dl.xonotic.org/xonotic-0.8.1.zip) (940M, md5: f7a9cd8ab68a00336acca164f983b569)  

[Download 0.8.0 via torrent](http://dl.xonotic.org/xonotic-0.8.0.zip.torrent) (953M, md5: bc368e116a2502362e1d4f07d8f8efab)  
[Download 0.8.0 via HTTP](http://dl.xonotic.org/xonotic-0.8.0.zip) (953M, md5: bc368e116a2502362e1d4f07d8f8efab)

[Download 0.7.0 via torrent](http://dl.xonotic.org/xonotic-0.7.0.zip.torrent) (993M, md5: eda7e8acadbefaf4b2efcfb70bbe98e2)  
[Download 0.7.0 via HTTP](http://dl.xonotic.org/xonotic-0.7.0.zip) (993M, md5: eda7e8acadbefaf4b2efcfb70bbe98e2)  

[Download 0.6.0 via torrent](http://dl.xonotic.org/xonotic-0.6.0.zip.torrent) (943M, md5: 2dac2c1ad4388255d3ad4d038dea3f77)  
[Download 0.6.0 via HTTP](http://dl.xonotic.org/xonotic-0.6.0.zip) (943M, md5: 2dac2c1ad4388255d3ad4d038dea3f77)  

[Download 0.5.0 via torrent](http://dl.xonotic.org/xonotic-0.5.0.zip.torrent) (943M, md5: cdadb384ccf9cad926bb377312832c2f)  
[Download 0.5.0 via HTTP](http://dl.xonotic.org/xonotic-0.5.0.zip) (943M, md5: cdadb384ccf9cad926bb377312832c2f)  

[Download 0.1.0 via torrent](http://dl.xonotic.org/xonotic-0.1.0preview.zip.torrent) (1.3G, md5: aafb43893aa66e01488c817e3a60d96d)  
[Download 0.1.0 via HTTP](http://dl.xonotic.org/xonotic-0.1.0preview.zip) (1.3G, md5: aafb43893aa66e01488c817e3a60d96d)  
